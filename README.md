# DIY智能鱼缸控制软件轻量版fishaiLite

#### 介绍

基于ESP32或ESP8266物联网模块作为核心控制器，最多可控制10路继电器。
基于Blinker库实现点灯科技的App远程控制，随时掌握心爱的鱼缸动态。

输入： 

1. 一路温度检测，支持DS18B20温度传感器。
2. 一路水位检测，支持浮球。

输出：

1. 在0.96寸OLED显示屏上实时时间、温度、水位情况。
2. 根据水位检测，自动启动补水泵，实现自动补水。此功能可关闭。
3. ESP32最多可控制10路继电器，ESP8266最多可控制X路继电器。
4. 基于点灯科技的App远程控制。

#### 软件架构

本项目采用VSCode + PIO插件开发，经过简单修改也可以采用Arduino IDE进行编译。
依赖库有: blinker, NTPClient, U8g2, OneWire, DallasTemperature

#### 安装教程

建议采用VSCode + PIO进行编译。
修改main.cpp里面的Wifi账号密码和点灯App的密钥即可。

```
const char *wifi_ssid = "wifi账号";
const char *wifi_pswd = "wifi密码";
const char *blinker_auth = "点灯科技App的设备密钥";
```

高级功能需定制：

1. 显示你的名字或自定义的文字
2. 通过连接ESP32的热点，可在网页上自由更改密钥和Wifi密码
3. 定时开关灯功能
4. 控制制冷片正负极转换，实现冷热切换自动恒温
5. 连接高清IPS显示屏，显示更加丰富的信息

#### 使用说明

自备硬件：

1. ESP32开发板或者ESP8266开发板 1个
2. 继电器 ESP32支持10个
3. 温度传感器(DS18B20) 1个
4. 水位检测浮球 1个
5. 0.96英寸OLED 4脚显示屏 1个
6. 电源，5V降压模块
7. 连接线材，如杜邦线，电线等
8. 其他被控设备：如灯，水泵灯

继电器和引脚对应：（源码默认指定的情况）

1. D13 -> 温度传感器(DS18B20)  点灯App上组件键名 temp
2. D4  -> 浮球接线。浮球默认接负极
3. D32 -> 补水泵
4. D33 -> 开关1 点灯App上组件键名 k1
5. D25 -> 开关2 点灯App上组件键名 k2
6. D26 -> 开关3 点灯App上组件键名 k3
7. D27 -> 开关4 点灯App上组件键名 k4
8. D14 -> 开关5 点灯App上组件键名 k5
9. D22 -> 开关6 点灯App上组件键名 k6
10. D21 -> 开关7 点灯App上组件键名 k7
11. D19 -> 开关8 点灯App上组件键名 k8
12. D18 -> 开关9 点灯App上组件键名 k9
13. D5  -> 开关10 点灯App上组件键名 k10

点灯App组件默认界面配置代码：

```
{¨version¨¨2.0.0¨¨config¨{¨headerColor¨¨transparent¨¨headerStyle¨¨dark¨¨background¨{¨img¨¨assets/img/headerbg.jpg¨¨isFull¨«}}¨dashboard¨|{¨type¨¨btn¨¨ico¨¨fad fa-lightbulb-on¨¨mode¨É¨t0¨¨鱼缸照明¨¨t1¨¨文本2¨¨bg¨É¨cols¨Ë¨rows¨Ë¨key¨¨k1¨´x´Í´y´Ê¨lstyle¨É¨clr¨¨#076EEF¨}{ßCßDßE¨fal fa-power-off¨ßGÊßH¨开关3¨ßJßKßLÉßMËßNËßO¨k3¨´x´É´y´ÒßQÊßR¨#EA0909¨}{ßCßDßEßTßGÉßH¨开关4¨ßJßKßLÉßMËßNËßO¨k4¨´x´Ë´y´ÒßQÉßR¨#00A90C¨}{ßCßDßE¨fad fa-siren¨ßGÉßH¨开关5¨ßJßKßLÉßMËßNËßO¨k5¨´x´Í´y´ÒßR¨#FBA613¨}{ßCßDßE¨fad fa-cloud-rain¨ßGÉßH¨开关6¨ßJßKßLÉßMËßNËßO¨k6¨´x´Ï´y´ÒßR¨#595959¨}{ßCßDßE¨fad fa-temperature-up¨ßGÉßH¨开关7¨ßJßKßLÉßMËßNËßO¨k7¨´x´É´y´¤BßRßZ}{ßCßDßE¨fad fa-temperature-down¨ßGÉßH¨开关8¨ßJßKßLÉßMËßNËßO¨k8¨´x´Ë´y´¤BßRßS}{ßCßDßE¨fad fa-fan-table¨ßGÉßH¨开关9¨ßJßKßLÉßMËßNËßO¨k9¨´x´Í´y´¤BßR¨#6010E4¨}{ßCßDßEßTßGÉßH¨开关10¨ßJßKßLÉßMËßNËßO¨k10¨´x´Ï´y´¤BßRßd}{ßCßDßE¨fad fa-siren-on¨ßGÉßH¨藻盒灯光¨ßJßKßLÉßMËßNËßO¨k2¨´x´Ï´y´ÊßQÉßRßZ}{ßC¨deb¨ßGÉßLÉßMÑßNÌßO¨debug¨´x´É´y´¤E}{ßC¨num¨ßH´水温´ßE¨fad fa-thermometer-three-quarters¨ßRßd¨min¨É¨max¨£U.84¨uni¨´度´ßLÉßMÍßNÍßO¨temp¨´x´É´y´ÊßQÍ´rt´»}{ßC¨cha¨ßLÉ¨sty¨¨line¨ßRßZ¨sty1¨ß17¨clr1¨¨#389BEE¨¨sty2¨ß17¨clr2¨ß1AßMÑßNÍßO¨tempChart¨´x´É´y´ÎßQÊ¨key0¨¨tempHis¨ßH´水温´}{ßC¨tex¨ßH¨鱼缸水位¨ßJß1H¨size¨´25´ßLÉßE¨fad fa-sprinkler¨ßMÍßNËßO¨water¨´x´Í´y´ÌßQÌßRßZ}÷¨actions¨|¦¨cmd¨¦¨switch¨‡¨text¨‡´on´¨打开?name¨¨off¨¨关闭?name¨—÷¨triggers¨|{¨source¨ß1N¨source_zh¨¨开关状态¨¨state¨|´on´ß1Q÷¨state_zh¨|´打开´´关闭´÷}÷´rt´|ß14÷}
```

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
