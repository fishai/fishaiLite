
/**
 * 全局定义头文件 define.h
 * 1. 引脚定义
 * 2. 常量定义
 */

#ifndef DEFINE_H
#define DEFINE_H

#define _VERSION "0.0.1 beta"
#define _MODDATE "2023-02-11"

// 检测周期，毫秒
#define MONITOR_MS 500

// 设定继电器触发电平 LOW/HIGH 注意和继电器硬件对应
#define RELAY_ON    LOW
#define RELAY_OFF   HIGH

// 是否支持补水功能。 补水需接浮球和补水泵. 改成0则不判断水位
#define SUPPORT_FEED_WATER 1

// ESP32 引脚设定
#if defined(ESP32)
  // 简单的OLED 4脚屏幕引脚设定
  #define OLED_SCK_PIN 15 //D15 输出: 屏幕
  #define OLED_SDA_PIN 2  //D2  输出: 屏幕

  // 温度检测引脚
  #define TEMP_PIN  13 //D13 输入, 点灯App上组件键名 temp

  // 补水引脚设定
  #define WATER_LEVEL_PIN     4       //D4 输入: 水位浮球 接负极。低电平触发!
  #define FEED_WATER_PUMP_PIN 32      //D32 输出: 控制补水泵继电器

  // 继电器控制引脚设定 TXD, RXD, TX2, RX2不可用
  #define K1_PIN    33 // D33 输出, 点灯App上组件键名 k1
  #define K2_PIN    25 // D25 输出, 点灯App上组件键名 k2
  #define K3_PIN    26 // D26 输出, 点灯App上组件键名 k3
  #define K4_PIN    27 // D27 输出, 点灯App上组件键名 k4
  #define K5_PIN    14 // D14 输出, 点灯App上组件键名 k5
  #define K6_PIN    22 // D22 输出, 点灯App上组件键名 k6
  #define K7_PIN    21 // D21 输出, 点灯App上组件键名 k7
  #define K8_PIN    19 // D19 输出, 点灯App上组件键名 k8
  #define K9_PIN    18 // D18 输出, 点灯App上组件键名 k9
  #define K10_PIN    5 // D5 输出, 点灯App上组件键名 k10
  
  // 启动时继电器默认值
  #define K1_INIT_VAL RELAY_OFF
  #define K2_INIT_VAL RELAY_OFF
  #define K3_INIT_VAL RELAY_OFF
  #define K4_INIT_VAL RELAY_OFF
  #define K5_INIT_VAL RELAY_OFF
  #define K6_INIT_VAL RELAY_OFF
  #define K7_INIT_VAL RELAY_OFF
  #define K8_INIT_VAL RELAY_OFF
  #define K9_INIT_VAL RELAY_OFF
  #define K10_INIT_VAL RELAY_OFF

// ESP8266 引脚设定
#elif defined(ESP8266)
  #error "Not support esp8266"

#else
  #error "please set Esp32"
#endif

#endif
