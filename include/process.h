/**
 * 数据处理控制接口
 * 
 * 
 */

#ifndef PROCESS_H
#define PROCESS_H

#include <Arduino.h>
#include "input.h"
#include "output.h"

namespace Process
{
    // 执行控制器
    bool run(String timeStr);

    // 继电器动作
    String relayAct(uint8_t pin, const String &state);

    String getRelayState(uint8_t pin); //获得继电器状态
};
#endif
