/**
 * 输出控制接口
 * 
 * 
 */

#ifndef OUTPUT_H
#define OUTPUT_H

#include <Arduino.h>
#include <U8g2lib.h> // OLED绘图库

#include "define.h"
#include "input.h"

namespace Output
{
    void begin();

    void loading();

    void chkWifi(String ssid, String pswd, String msg);

    void run(TEMP_VALUE &temp, int water, String timeStr);

    void ledOn();

    void ledOff();

    void ledFlashCallback();

    void ledFlash(uint32_t loopMs = 200, uint32_t continueSec = 0);
};
#endif