/**
 * 初始化接口
 * 
 * 
 */

#ifndef SETUP_H
#define SETUP_H

#include <Arduino.h>
#include <WiFi.h>

#include "input.h"
#include "output.h"

namespace Setup
{
    // 开始设置
    void begin();

    // 检测Wifi
    bool chkWifi(WiFiClass *wifi, const char *ssid, const char *pswd);
};
#endif