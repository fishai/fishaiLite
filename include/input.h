/**
 * 输入控制接口
 * 
 * 
 */

#ifndef INPUT_H
#define INPUT_H

#include <Arduino.h>
#include "define.h"

#include <DallasTemperature.h> //温度传感器

#define WATER_LOW       0 // 水位低
#define WATER_NORMAL    1 // 水位高

// 温度值
struct TEMP_VALUE
{
    bool valid; //是否有效
    double value; //摄氏度值
};

namespace Input
{
    // 输入引脚初始化
    void begin();

    // 采集温度值
    TEMP_VALUE getTemp();

    // 采集水位
    int getWaterLevel();  
    
};
#endif
