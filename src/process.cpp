
#include "process.h"
//#include <Arduino_JSON.h>

namespace Process
{
    unsigned long last_do_time = 0;
};

bool Process::run(String timeStr)
{
    if( millis() - last_do_time < MONITOR_MS )
    {
        return false;
    }

    // 温度采集
    TEMP_VALUE temp = Input::getTemp();

    // 水位采集
    int water = -1; // 默认-1，表示不处理
  #if SUPPORT_FEED_WATER
    water = Input::getWaterLevel();
    if(water == WATER_LOW)
    {
        digitalWrite(FEED_WATER_PUMP_PIN, RELAY_ON); //低水位，打开开关
        //Serial.println("water RELAY_ON");
    }
    else
    {
        digitalWrite(FEED_WATER_PUMP_PIN, RELAY_OFF);
        //Serial.println("water RELAY_OFF");
    }
  #endif

    // 显示数据
    Output::run(temp, water, timeStr);

    last_do_time = millis();
    return true;
}

String Process::relayAct(uint8_t pin, const String &state)
{
    int oldVal = digitalRead(pin);
    int newVal = oldVal;
    if(state.equals("on")) // 开
    {
        newVal = RELAY_ON;
    }
    else if(state.equals("off")) // 关
    {
        newVal = RELAY_OFF;
    }
    else// if(state.equals("tap")) // 普通按键
    {
        newVal = !oldVal;
    }
    digitalWrite(pin, newVal);
    Serial.println( String("relayAct: ") + String(pin) +" => "+ String(newVal) );
    return newVal == RELAY_ON ? "on" : "off";
}

String Process::getRelayState(uint8_t pin) //获得继电器状态
{
    int val = digitalRead(pin);
    return val == RELAY_ON ? "on" : "off";
}
