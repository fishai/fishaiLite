
#include "output.h"
#include "bmp/loading.h"
#include "bmp/conn-wifi.h"
#include "bmp/water-low.h"
#include "bmp/water-normal.h"
#include <Ticker.h>

namespace Output
{
    // 显示接口
    U8G2_SSD1306_128X64_NONAME_1_SW_I2C u8g2(U8G2_R0, /* clock=*/OLED_SCK_PIN, /* data=*/OLED_SDA_PIN, /* reset=*/U8X8_PIN_NONE);

    Ticker ledTicker;
    Ticker ledStop;
};

// 开始
void Output::begin()
{
    // 引脚配置
    pinMode(LED_BUILTIN, OUTPUT); //板载LED
    pinMode(FEED_WATER_PUMP_PIN, OUTPUT); //补水泵 继电器引脚
    pinMode(K1_PIN, OUTPUT); //其他继电器引脚
    pinMode(K2_PIN, OUTPUT);
    pinMode(K3_PIN, OUTPUT);
    pinMode(K4_PIN, OUTPUT);
    pinMode(K5_PIN, OUTPUT);
    pinMode(K6_PIN, OUTPUT);
    pinMode(K7_PIN, OUTPUT);
    pinMode(K8_PIN, OUTPUT);
    pinMode(K9_PIN, OUTPUT);
    pinMode(K10_PIN, OUTPUT);

    // 初始值
    digitalWrite(FEED_WATER_PUMP_PIN, RELAY_OFF);
    digitalWrite(K1_PIN, K1_INIT_VAL);
    digitalWrite(K2_PIN, K2_INIT_VAL);
    digitalWrite(K3_PIN, K3_INIT_VAL);
    digitalWrite(K4_PIN, K4_INIT_VAL);
    digitalWrite(K5_PIN, K5_INIT_VAL);
    digitalWrite(K6_PIN, K6_INIT_VAL);
    digitalWrite(K7_PIN, K7_INIT_VAL);
    digitalWrite(K8_PIN, K8_INIT_VAL);
    digitalWrite(K9_PIN, K9_INIT_VAL);
    digitalWrite(K10_PIN, K10_INIT_VAL);

    // 绘图
    u8g2.begin();
    u8g2.enableUTF8Print(); // enable UTF8 support for the Arduino print() function
}

// 加载界面
void Output::loading()
{
    u8g2.firstPage();
    do
    {
        u8g2.drawXBM(0, 0, loading_bmp_w, loading_bmp_h, loading_bmp);
    } while (u8g2.nextPage());
}

// 显示连接网络界面
void Output::chkWifi(String ssid, String pswd, String msg)
{
    u8g2.firstPage();
    do
    {
        u8g2.drawXBM(0, 0, connwifi_bmp_w, connwifi_bmp_h, connwifi_bmp);

        u8g2.setFont(u8g2_font_6x12_tf);
        u8g2.setCursor(10, connwifi_bmp_h+12);
        u8g2.print( String("wifi: ")+ ssid);

        u8g2.setCursor(10, connwifi_bmp_h+15+12);
        u8g2.print( String("pswd: ")+ pswd);

        u8g2.setCursor(10, 60);
        u8g2.print(msg);

    } while (u8g2.nextPage());
}

// 显示监视界面
void Output::run(TEMP_VALUE &temp, int water, String timeStr)
{
    u8g2.firstPage();
    do
    {
        // 左上角 时间
        u8g2.setFont(u8g2_font_6x12_tf);
        u8g2.setCursor(0, 12);
        u8g2.print(timeStr.substring(0,5)); // 不显示秒。 屏幕刷新太慢

        // 右上角 水位检测
        if( water > -1 )
        {
            if( water == WATER_LOW)
                u8g2.drawXBM(68, 0, waterlow_bmp_w, waterlow_bmp_h, waterlow_bmp);
            else
                u8g2.drawXBM(68, 0, waternormal_bmp_w, waternormal_bmp_h, waternormal_bmp);
        }
        else 
        {
            u8g2.setCursor(60, 12);
            u8g2.print("FishAi.cn");
        }

        // 中间 温度显示
        u8g2.setFont(u8g2_font_fub35_tf);
        u8g2.setCursor(0, 64);
        if( temp.valid )
            u8g2.print( temp.value);
        else
            u8g2.print("Err");

    } while (u8g2.nextPage());
}

// 控制LED闪烁
void Output::ledFlashCallback()
{
    static int v = HIGH;
#ifdef ESP32
    digitalWrite(LED_BUILTIN, v);
#else
    digitalWrite(LED_BUILTIN, v);
#endif
    v = v == HIGH ? LOW : HIGH;
}

// LED常亮
void Output::ledOn()
{
    if (ledTicker.active())
        ledTicker.detach();
    if (ledStop.active())
        ledStop.detach();
#ifdef ESP32
    digitalWrite(LED_BUILTIN, HIGH);
#else
    digitalWrite(LED_BUILTIN, LOW);
#endif
}

// LED灭
void Output::ledOff()
{
    if (ledTicker.active())
        ledTicker.detach();
    if (ledStop.active())
        ledStop.detach();
#ifdef ESP32
    digitalWrite(LED_BUILTIN, LOW);
#else
    digitalWrite(LED_BUILTIN, HIGH);
#endif
}

// LED闪烁，闪烁周期毫秒loopMs, 持续时间秒:0不停止
void Output::ledFlash(uint32_t loopMs, uint32_t continueSec)
{
    if (ledTicker.active())
    {
        ledTicker.detach();
    }
    ledTicker.attach_ms(loopMs, ledFlashCallback);
    if (continueSec > 0)
        ledStop.once(continueSec, ledOff);
}
