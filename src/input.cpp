#include "input.h"

extern DallasTemperature waterTemp;

// 输入引脚初始化
void Input::begin()
{
    pinMode(WATER_LEVEL_PIN, INPUT_PULLUP);

}

// 采集温度值
TEMP_VALUE Input::getTemp()
{
    const uint8_t idx = 0; //默认取第一个温度的
    TEMP_VALUE val;
    if (waterTemp.requestTemperaturesByIndex(idx))
    {
        val.value = waterTemp.getTempCByIndex(idx);
        val.valid = val.value > 0;
    }
    else
    {
        val.value = 0;
        val.valid = false;
    }
    return val;
}

// 采集水位
int Input::getWaterLevel()
{
    int value = digitalRead(WATER_LEVEL_PIN); 
    int state = value == LOW ? WATER_LOW : WATER_NORMAL; // 低电平接通
    return state;
}
