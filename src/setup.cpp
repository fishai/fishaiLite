
#include "setup.h"

// 启动设置
void Setup::begin()
{
    // 输出引脚
    Output::begin();
    Serial.println("Output::begin");

    // 输入引脚
    Input::begin();
    Serial.println("Input::begin");

    // 显示加载中
    Output::loading();
}

// 连接网络
bool Setup::chkWifi(WiFiClass *wifi, const char *ssid, const char *pswd)
{
    int waitCount = 0;
    bool suc = false;
    for (waitCount = 1; waitCount <= 100; waitCount++)
    {
      wl_status_t s = wifi->status();
      if (s == WL_CONNECT_FAILED)
      {
        suc = false;
        break;
      }
      else if (s == WL_CONNECTED)
      {
        suc = true;
        break;
      }
      String msg = String("connect wifi:")+ String(waitCount)+"%";
      Output::chkWifi(ssid, pswd, msg);
      Serial.println("chkWifi");
      delay(300); // 循环100次，30秒超时
    }

    String msg = String("connect wifi:") + String(suc?" OK":" ERR");
    Output::chkWifi(ssid, pswd, msg);
    return suc;
}

